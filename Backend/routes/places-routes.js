const express = require("express");
const { check } = require("express-validator"); // both syntaxes work

const placesControllers = require("../controllers/places-controllers");

const router = express.Router();

// order of routes matter!

// first one: handles all requests that start with :pid/ANY VALUE
router.get("/:pid", placesControllers.getPlaceById);

// second: handles all requests with /api/places/user/:uid since this has two segments
router.get("/user/:uid", placesControllers.getPlacesByUserId);

router.post(
  "/",
  [
    check("title").not().isEmpty(),
    check("description").isLength({ min: 5 }),
    check("address").not().isEmpty(),
  ],
  placesControllers.createPlace
);

router.patch(
  "/:pid",
  [check("title").not().isEmpty(), check("description").isLength({ min: 5 })],
  placesControllers.updatePlace
);

router.delete("/:pid", placesControllers.deletePlace);

module.exports = router;
