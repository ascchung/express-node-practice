const uuid = require("uuid").v4;
const { validationResult } = require("express-validator");

const HttpError = require("../models/http-error");
const User = require("../models/user");

// let DUMMY_USERS = [
//   {
//     id: "u1",
//     name: "Andrew Chung",
//     email: "user@example.com",
//     password: "test",
//   },
// ];

const getUsers = async (req, res, next) => {
  let users;
  try {
    users = await User.find({}, "-password");
  } catch (err) {
    const error = new HttpError("Fetching users failed, try again.", 500);
    return next(error);
  }
  res.json({ users: users.map((user) => user.toObject({ getters: true })) });
};

const signup = async (req, res, next) => {
  const errors = validationResult(req); // look into request object to see any errors based on setup in routes
  if (!errors.isEmpty()) {
    return next(new HttpError("Invalid inputs, please check data", 422));
  }
  const { name, email, password } = req.body;

  // const hasUser = DUMMY_USERS.find((u) => u.email === email);
  // if (hasUser) {
  //   throw new HttpError("Could not create user, email exists.", 422);
  // }

  let existingUser;
  try {
    existingUser = await User.findOne({ email: email });
  } catch (err) {
    const error = new HttpError("Signup failed, try again", 500);
    return next(error);
  }

  if (existingUser) {
    const error = new HttpError("User exists, please login instead", 422);
    return next(error);
  }

  const createdUser = new User({
    name,
    email,
    image: "https://live.staticflicker.com/7631/26849088292_36fc52ee90_b.jpg",
    password,
    places: [],
  });

  // const createdUser = {
  //   id: uuid(),
  //   name,
  //   email,
  //   password,
  // };

  // DUMMY_USERS.push(createdUser);

  try {
    await createdUser.save(); // mongodb save handler with unique id
  } catch (err) {
    const error = new HttpError("Creating user failed, please try again.", 500);
    return next(error); // this is so that code stops if there is an error
  }

  res.status(201).json({ user: createdUser.toObject({ getters: true }) }); //getters removes underscore ID. toObject helps turn it into default JS object
};

const login = async (req, res, next) => {
  const { email, password } = req.body;

  let existingUser;
  try {
    existingUser = await User.findOne({ email: email });
  } catch (err) {
    const error = new HttpError("Login failed, try again", 500);
    return next(error);
  }

  if (!existingUser || existingUser.password !== password) {
    // if existing user doesn't exist, or password is incorrect
    const error = new HttpError("Invalid credentials, could not login", 401);
    return next(error);
  }

  // const identifiedUser = DUMMY_USERS.find((u) => u.email === email);
  // if (!identifiedUser || identifiedUser.password !== password) {
  //   throw new HttpError("Could not identify user.", 401);
  // }
  res.json({ message: "Logged in!" });
};

exports.getUsers = getUsers;
exports.signup = signup;
exports.login = login;
