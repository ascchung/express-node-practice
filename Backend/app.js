const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const placesRoutes = require("./routes/places-routes");
const usersRoutes = require("./routes/users-routes");
const HttpError = require("./models/http-error");

const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE");
  next();
});

app.use("/api/places", placesRoutes);
app.use("/api/users", usersRoutes);

app.use((req, res, next) => {
  const error = new HttpError("Could not find this route.", 404);
  throw error;
});

app.use((error, req, res, next) => {
  if (res.headerSent) {
    return next(error);
  }
  res
    .status(error.code || 500)
    .json({ message: error.message || "An unknown error occurred!" });
}); // special middleware as error handler, only will execute if any middleware in front has an error

mongoose
  .connect(
    "mongodb+srv://ascchung:022410@cluster0.qhkso0v.mongodb.net/places?retryWrites=true&w=majority&appName=Cluster0",
    {
      useNewUrlParser: true, // Use the new URL parser
      useUnifiedTopology: true, // Use the new Server Discover and Monitoring engine
      writeConcern: {
        w: "majority",
        wtimeout: 5000,
      },
    }
  )
  .then(() => {
    app.listen(8000);
    console.log("Connected to MongoDB and Server is running on port 8000");
  })
  .catch((e) => {
    console.error("Connection failed:", e);
  });
